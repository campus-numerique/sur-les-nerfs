# AngularProject

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.1.1.


## Installation de l'environnement Angular CLI

**Prérequis :**
+ node.js **6.9.0** ou plus
+ npm **3.0.0** ou plus

Pour verifier
````
node -v
npm -v
````
**Si vous n'avez pas les bonnes versions :** 
````
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
[sudo] apt-get install -y nodejs
````

Une fois que tout est bon, ouvrer un terminal dans le **project-angular** 
````
npm install
ng serve
````
## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
