import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import {Router, ActivatedRoute, Params} from '@angular/router';

import { PostRepositoryService } from './../post/post-repository.service';
import { CommentService } from './../comment/comment.service';

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.css']
})
export class PostDetailComponent implements OnInit {
  commentForm: FormGroup;
  postDetail: any[];
  comments: any[];
  comment: any;
  error: string = '';
  constructor(
    private postRepository: PostRepositoryService,
    private commentService:  CommentService,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder, 
    private router: Router
  ) {
      this.commentForm = formBuilder.group({
            'pseudo': ['', Validators.required],
            'content': ['', Validators.required]
        });
   }
  
  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      let id = params['id'];
    
      this.postRepository
        .getOnePost(id)
        .subscribe(
          postDetail => this.postDetail = postDetail,
          error => this.error = error
        );
      this.commentService
        .getCommentByPost(id)
        .subscribe(
          comments => this.comments = comments,
          error => this.error = error
        );
    });
  }

  addComment(id) {
    this.commentService
      .createComment(this.commentForm.value, id)
      .subscribe(
          data => {
            //this.router.navigate(['postDetail']);
          },
          error => console.log(error)
          );
      window.location.reload();
  }

}
