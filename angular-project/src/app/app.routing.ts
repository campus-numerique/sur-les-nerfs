import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { HomepageComponent } from './homepage/homepage.component';
import { AuthenticationComponent } from './authentication/authentication.component';
import { PostComponent } from './post/post.component';
import { PostCreateComponent } from './post/postCreate.component';
import { AuthGuard } from './_guard/index';
import { PostDetailComponent } from "./post-detail/post-detail.component";
import { PostUpdateComponent } from "./post-update/post-update.component";
import { CommentComponent } from "./comment/comment.component";

const APP_ROUTES: Routes = [
	{
		path: '',
		component: HomepageComponent
	},
	{
		path: 'login',
		component: AuthenticationComponent
	},
	{
		path: 'post',
		component: PostComponent
	},
	{
		path: 'post/create',
		component: PostCreateComponent,
		// canActivate: [AuthGuard]  // protege la route
	},
	{
		path: 'post/:id',
		component: PostDetailComponent,
		// canActivate: [AuthGuard]  // protege la route
	},
	{
		path: 'post/update/:id',
		component: PostUpdateComponent,
	},
	{
		path: 'comment',
		component: CommentComponent
	},
	{ path: '**', redirectTo: '' }
];
 
export const Routing: ModuleWithProviders = RouterModule.forRoot(APP_ROUTES);
