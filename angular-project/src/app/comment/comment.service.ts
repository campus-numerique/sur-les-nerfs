import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { environment } from "environments/environment";

@Injectable()
export class CommentService {
  private commentUrl = 'http://127.0.0.1/post';

  constructor(
    private http: Http
  ) { }

  getAllComments(): Observable<any[]> {
    return this.http.get(this.commentUrl)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'))
  }

  getCommentByPost(idPost:number): Observable<any> {
    return this.http.get(this.commentUrl + "/" + idPost + "/comment")
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'))
  }

  createComment(objet: Object, id: number): Observable<any> {
  	let body = JSON.stringify(objet);
  	return this.http.post(this.commentUrl + '/' + id + '/createComment', body)
  		.map((res:Response) => res.json())
      	.catch((error:any) => Observable.throw(error.json().error || 'Server error'))
  }
}
