import { Component, OnInit } from '@angular/core';
import { CommentService } from "./comment.service";

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {
  comments: any[];
  comment: any;
  error: string = '';

  constructor(
    private commentService: CommentService
  ) { }

  ngOnInit() {
    this.commentService
      .getAllComments()
      .subscribe(
        comments => this.comments = comments,
        error => this.error = error
      )
  }

}
