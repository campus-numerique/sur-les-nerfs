import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { PostRepositoryService } from './../post/post-repository.service';
@Component({
  selector: 'app-post-update',
  templateUrl: './post-update.component.html',
  styleUrls: ['./post-update.component.css']
})
export class PostUpdateComponent implements OnInit {
  postDetail: any[];
  error: string = '';
  postUpdateForm: FormGroup;

  constructor(
    private postRepository: PostRepositoryService,
    private formBuilder: FormBuilder, 
    private activatedRoute: ActivatedRoute,
		private router: Router
    ) {
        this.postUpdateForm = formBuilder.group({
            'title': [],
            'content': [],
            'url_image': []
        });
    }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      let id = params['id'];
    
      this.postRepository
        .getOnePost(id)
        .subscribe(
          postDetail => this.postDetail = postDetail,
          error => this.error = error
        );
    });
  }

  updatePost(id: number)
  {
    this.postRepository
      .update(id, this.postUpdateForm.value)
      .subscribe(
                data => {
                   this.router.navigate(['post']);
                },
				error => this.error = error
			);
  }

}
