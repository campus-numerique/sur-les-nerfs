import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
 
import { PostRepositoryService } from './post-repository.service';
@Component({
  selector: 'app-post',
  templateUrl: './postCreate.component.html',
  styleUrls: ['./postCreate.component.css']
})
export class PostCreateComponent{
    postForm: FormGroup;
	posts: any[];
	error: string = '';
	
	constructor(
        private postRepository: PostRepositoryService,
        private formBuilder: FormBuilder, 
		private router: Router
    ) {
        this.postForm = formBuilder.group({
            'title': ['', Validators.required],
            'post': ['', Validators.required],
            'url_image': ['', Validators.required]
        });
    }

	addPost() {
		this.postRepository
			.createPost(this.postForm.value) 
			.subscribe(
                data => {
                    this.router.navigate(['post']);	
                    console.log('pas derreur');
                },
				error => console.log(error)
			);	
        this.router.navigate(['post'])
    }
}