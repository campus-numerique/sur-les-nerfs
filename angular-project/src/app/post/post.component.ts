import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
 
import { PostRepositoryService } from './post-repository.service';
import { CommentService } from "app/comment/comment.service";
@Component({
  	selector: 'app-post',
  	templateUrl: './post.component.html',
	styleUrls: ['./post.component.css']

})
export class PostComponent implements OnInit {
	posts: any[];
	error: string = '';
	comments: any[];
	
 
	constructor(
		private postRepository: PostRepositoryService,
		private router: Router,
		private commentService: CommentService
	) {}
		
 
	ngOnInit() {
		this.postRepository
			.getPosts()
			.subscribe(
	            posts => this.posts = posts,
	            error => this.error = error.message
	        );
	}

	deletePost(id :number){
		this.postRepository
			.deletePost(id)
			.subscribe(
                data => {this.router.navigate(['post']);	
                },
				error => console.log(error)
			);
			window.location.reload();
	}
}