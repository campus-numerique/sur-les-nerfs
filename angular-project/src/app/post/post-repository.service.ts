import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import { environment } from "environments/environment";
 
@Injectable()
export class PostRepositoryService {
  private postUrl = 'http://127.0.0.1/post';
  private postUpdate = 'http://127.0.0.1/post/update';  
  private postCreate = 'http://127.0.0.1/create';
  private postDelete = 'http://127.0.0.1/delete';
  private headers = new Headers({ 'Content-Type': 'application/json' });

  constructor(
    private authHttp: AuthHttp, 
    private http: Http
  ) {}

  getPosts(): Observable<any[]> {
    return this.http.get(this.postUrl)
                    // ...and calling .json() on the response to return data
                    .map((res:Response) => res.json())
                    //...errors if any
                    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  createPost(objet: Object): Observable<any> {
    // this.headers.append('Authorization',"Basic " + btoa(environment.username + ':' + environment.password));
    let body = JSON.stringify(objet);
    return this.http.post(this.postCreate, body)
                    // ...and calling .json() on the response to return data
                    .map((res:Response) => res.json())
                    //...errors if any
                    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  deletePost(id): Observable<any> {
    //this.headers.append('Authorization',"Basic " + btoa(environment.username + ':' + environment.password));
    let url = this.postDelete + '/' + id;
    return this.http.delete(url)
                    .map((res:Response) => res.json())
                    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  getOnePost(id): Observable<any[]> {
    let url = this.postUrl+'/'+id; 
    return this.http.get(url)
                    // ...and calling .json() on the response to return data
                    .map((res:Response) => res.json())
                    //...errors if any
                    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  update(id, objet: Object): Observable<any>{
    //this.headers.append('Authorization',"Basic " + btoa(environment.username + ':' + environment.password));
    let body = JSON.stringify(objet);
    let url = this.postUpdate +'/' + id;
    console.log(url + body); 
    return this.http.put(url, body)
                    // ...and calling .json() on the response to return data
                    .map((res:Response) => res.json())
                    //...errors if any
                    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
}