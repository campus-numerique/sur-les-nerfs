### sur-les-nerfs
Blog with micka, amine, boris, kevin

Boris:
- [ ] Monter l'environnement angular 2
- [ ] Créer une directive
- [ ] Lire une API
- [ ] Envoyer les données d'un formulaire à une API.
- [ ] Relier l'affichage à des données entrantes (binding)
- [ ] Créer des routes

Amine:
- [ ] Créer un bundle
- [ ] Utiliser doctrine
- [ ] Créer une page
- [ ] Créer un formulaire
- [ ] Créer un service
- [ ] Appeler un service

Kévin:
- [ ] Monter l'environnement angular 2
- [ ] Créer une directive
- [ ] Lire une API
- [ ] Envoyer les données d'un formulaire à une API.
- [ ] Relier l'affichage à des données entrantes (binding)
- [ ] Créer des routes

Mickael:
- [ ] Créer un bundle
- [ ] Utiliser doctrine
- [ ] Créer une page
- [ ] Créer un formulaire
- [ ] Créer un service
- [ ] Appeler un service
- [ ] Débugger des erreurs courantes
- [ ] Mettre en place des relations entre les Models
- [ ] Sécuriser l’accès à son application via l’authentification - Authorisation accès aux données
- [ ] Sécuriser l’accès à son application via l’authentification - Données privatives
