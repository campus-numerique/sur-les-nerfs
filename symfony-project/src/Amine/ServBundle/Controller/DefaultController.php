<?php

namespace Amine\ServBundle\Controller;

use Amine\ServBundle\Services\MessageGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {

        return $this->render('AmineServBundle:Default:index.html.twig');

    }

    public function serviceAction(){

    		$messageGenerator = $this->container->get('app.message_generator');

    		$message = $messageGenerator->getHappyMessage();

	    return $this->render('AmineServBundle:Default:messages.html.twig', Array (
	    	'message' => $message 	

	   	));

	}
}
