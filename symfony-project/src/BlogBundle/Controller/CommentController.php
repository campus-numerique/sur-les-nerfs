<?php

namespace BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use BlogBundle\Entity\Post;
use BlogBundle\Entity\Comment;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Doctrine\ORM\EntityManagerInterface;

class CommentController extends Controller
{
    /**
     * @Route("/post/{id}/createComment" , name="createComment",requirements={"id"="\d+"})
     * @Method({"POST"})
     */
    public function createAction( Request $request, $id)
    {
            $post = $this->getDoctrine()
                ->getRepository('BlogBundle:Post')
                ->find($id);

            $comment = new Comment();
            $data = $request->getContent();
            $comment = $this->get('jms_serializer')->deserialize($data, 'BlogBundle\Entity\Comment', 'json');
            $comment->setDate(new \Datetime());
            //on lie  comment au post
            $post->addComment($comment);

            $em = $this->getdoctrine()->getmanager();
            $em->persist($comment);
            $em->flush();

            return new Response('', Response::HTTP_CREATED);       
    }

    /**
     * @Route("/delete")
     */
    public function deleteAction()
    {
        return $this->render('BlogBundle:Comment:delete.html.php', array(
            // ...
        ));
    }

   /**
     * @Route("/post/{id}/comment")
     * @Method({"GET"})
     */
    public function showAction($id)
    {
        $post = $this->getDoctrine()
                ->getRepository('BlogBundle:Post')
                ->find($id);
        $posts = $post->getComments();     

        $data = $this->get('jms_serializer')->serialize($posts, 'json');
        
        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

}
