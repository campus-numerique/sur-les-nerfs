<?php

namespace BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use BlogBundle\Entity\Post;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Doctrine\ORM\EntityManagerInterface;


class DefaultController extends Controller
{
    /**
     * @Route("/create")
     * @Method({"POST"})
     */
    public function createAction(Request $request )
    {
    		$post = new Post();

        	$data = $request->getContent();
        	$post = $this->get('jms_serializer')->deserialize($data, 'BlogBundle\Entity\Post', 'json');
        	$post->setDate(new \Datetime());
	        $em = $this->getdoctrine()->getmanager();
	        $em->persist($post);
	        $em->flush();

	        return new Response('', Response::HTTP_CREATED);
	}

	/**
     * @Route("/post", name="posts")
     * @Method({"GET"})
     */

	public function showAllPost(){

		$posts = $this->getDoctrine()
				->getRepository('BlogBundle:Post')
				->findAll();

	    $data = $this->get('jms_serializer')->serialize($posts, 'json');

        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
		
		
	}

	/**
     * @Route("/post/{id}" , name="post",requirements={"id"="\d+"})
     * @Method({"GET"})
     */

	public function showPost(Post $post){

 		$data = $this->get('jms_serializer')->serialize($post, 'json');
        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');

        return $response;

	}

	/**
     * @Route("/delete/{id}" , name="deletepost",requirements={"id"="\d+"})
     * @Method({"DELETE"})
     */

	public function deletePost ($id){
			
			$post = $this->getDoctrine()
				->getRepository('BlogBundle:Post')
				->find($id);

			if (!$post) {
            throw $this->createNotFoundException('Aucun post');
        	}

	        $em = $this->getdoctrine()->getmanager();
	        $em->remove($post);
	        $em->flush();


	    return new Response('', Response::HTTP_OK);

	}

	/**
     * @Route("/post/update/{id}" , name="updatepost",requirements={"id"="\d+"})
     * @Method({"PUT"})
     */

	public function updatePost (Request $request, $id){
			//on recupere le post a modifier avec son id
			$post = $this->getDoctrine()
				->getRepository('BlogBundle:Post')
				->find($id);

	        // on recupere les nouvelles données
	        $data = $request->getContent();
        	// 
        	$post = $this->get('jms_serializer')->deserialize($data, 'BlogBundle\Entity\Post', 'json');
         	echo var_dump($post);
         	echo "toto";
         	die();
			$post->setDate(new \Datetime());
			// $post->setTitle();
			// $post->setPost();
			// $post->seturl_image();
	        $em = $this->getdoctrine()->getmanager();
	        $em->persist($post);
	        $em->flush();

	        return new Response('', Response::HTTP_CREATED);
	}
}


