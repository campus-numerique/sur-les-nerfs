<?php

namespace BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Post
 *
 * @ORM\Table(name="post")
 * @ORM\Entity(repositoryClass="BlogBundle\Repository\FormRepository")
 */
class Post
{ 
    /**
     * @var int
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     */
    private $id;

    /**
     * @var string $title
     * @ORM\Column(name="title", type="text")
     */
    private $title;

    /**
     * @var \DateTime
     * @ORM\Column(name="date", type="datetime")
     */   
    private $date;

    /**
     * @var string $post
     *
     * @ORM\Column(name="content", type="text")
     */
    private $post;

    /**
     * @var string $url_image
     *
     * @ORM\Column(name="url_image", type="text", nullable=true)
     */
    private $url_image;

    /**
     * @ORM\OneToMany(targetEntity="BlogBundle\Entity\Comment", mappedBy="post")
     * @ORM\JoinColumn(nullable=false)
     */
    private $comments;


    public function __construct()
    {
        //$this->date = new \DateTime();
        $this->comments = new ArrayCollection();
    }



    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getPost()
    {
        return $this->post;
    }

    public function setPost($post)
    {
        $this->post = $post;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate($date)
    {
        $this->date = $date;
    }

    public function getUrl_image()
    {
        return $this->url_image;
    }

    public function setUrl_image($url_image)
    {
        $this->url_image = $url_image;
    }


    /**
     * Gets the value of comments.
     *
     * @return mixed
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Sets the value of comments.
     *
     * @param mixed $comments the comments
     *
     * @return self
     */
    public function addComment(Comment $comment)
    {
        $this->comments[] = $comment;

        //on lie le post au commentaire
        $comment->setPost($this);

        return $this;
    }

    public function removeComment(Comment $comment)
    {
        $this->comments->removeElement($comment);
    }
}

